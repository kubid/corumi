Rails.application.routes.draw do
  devise_for :users, only: [:omniauth_callbacks], :controllers => { :omniauth_callbacks => "corumiauth" }

  root 'home#index'
  get 'test1', to: "home#test1" 
  
  delete 'logout', to: "home#logout" 
end
