class CorumiauthController < ApplicationController

  def facebook
    # session[:user] = request.env['omniauth.auth']
    param = request.env['omniauth.auth']
    user = User.new(
      fb_username: param['info']['name'],
      email: param['info']['email'],
    )
    if user.save
      sign_in(user)
      flash[:success] = "berhasil login"
      redirect_to root_path
    else
      #  do something
    end
  end
end